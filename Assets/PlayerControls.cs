﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControls : MonoBehaviour {

	public KeyCode moveUp;
	public KeyCode moveDown;
	public float speed = 10f;
	private Rigidbody2D body;

	// Initialization calls.
	void Start() {
		body = GetComponent<Rigidbody2D> ();
	}

	// Update is called once per frame
	void Update () {

		// C# is more performant depending on how well off you are with Unity Engine (and programming in general).
		// JS (or UnityScript, rather) is easier to manage because it obfuscates object processes (such as Start, Update, Awake, etc.).
		// Overall, it's best to start and stay on C#. It's a learning curve but worth investing your time in.

		if (Input.GetKey (moveUp)) {
			// Below causes an error, but why?
			// body.velocity.y = speed;
			body.velocity = new Vector2 (0, speed);
		} else if (Input.GetKey (moveDown)) {
			body.velocity = new Vector2 (0, -speed);
		} else {
			body.velocity = Vector2.zero;
		}
	}
}
